#include <stdio.h>

int nod(int a, int b, int del);

int max(int a,int b);

int min(int a,int b);

void printer(int answer);

int main(){
	int a,b;
	scanf("%d%d",&a,&b);
	if(a<0){
		a=a*(-1);
	}
	if(b<0){
		b=b*(-1);
	}
	printer(nod(a,b,min(a,b)));
}

int nod(int a, int b,int del){
	if((a%del==0)&&(b%del==0)){
		return del;
	}
	if(del==1){
		return 1;
	}
	return nod(a,b,del-1);
}

int max(int a,int b){
	if (a<b)
		return b;
	return a;
}

int min(int a,int b){
	if(a>b)
		return b;
	return a;
}

void printer(int answer){
	printf("%d\n",answer);
}

