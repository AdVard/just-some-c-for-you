#include <stdio.h>
#include <stdlib.h>
#include <math.h>
typedef struct {
	double x;
	double y;
}Point;

int stratege();

void size_line();

void in_line();

void train();

void size_line(){
	Point a, b;
	scanf("%lf %lf", &a.x, &a.y);
	scanf("%lf %lf",&b.x, &b.y);
	double answer = (b.x-a.x)*(b.x-a.x)+(b.y-a.y)*(b.y-a.y);
	printf("%fl\n",sqrt(answer));
	stratege();
}

void in_line(){
	Point a, b,c;
	scanf("%lf %lf", &a.x, &a.y);
	scanf("%lf %lf",&b.x, &b.y);
	scanf("%lf %lf",&c.x,&c.y);
	if(((c.x-a.x)/(c.y-a.y))==((b.x-a.x)/(b.y-a.y))){
		printf("YES\n");
	}
	else{
		printf("NO\n");
	}
	stratege();
}

void train(){
	Point a, b,c,d;
	scanf("%lf %lf", &a.x, &a.y);
	scanf("%lf %lf",&b.x, &b.y);
	scanf("%lf %lf",&c.x,&c.y);
	scanf("%lf %lf",&d.x,&d.y);
	double s = ((a.x-d.x)*(b.y-a.y))-((b.x-a.x)*(b.y-d.y));
	double s1 = ((b.x-d.x)*(c.y-b.y))-((c.x-b.x)*(b.y-d.y));
	double s2 = ((c.x-d.x)*(a.y-c.y))-((a.x-c.x)*(c.y-d.y));
	if(((s>=0)&&(s1>=0)&&(s2>=0))||((s<=0)&&(s1<=0)&&(s2<=0))){
		printf("YES\n");
	}
	else{
		printf("NO\n");
	}
	stratege();
}

int stratege(){
	printf("Enter strat (lsize-1, line-2, trian-3, exit-4):");
	int ans;
	scanf("%d",&ans);
	if(ans==1){
		size_line();
	}
	if(ans==2){
		in_line();
	}
	if(ans==3){
		train();
	}
	if(ans==4){
		return 0;
	}
	
}

int main(){
	stratege();
	return 0;
}
