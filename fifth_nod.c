#include <stdio.h>

int nod(int a, int b);

int max(int a,int b);

int min(int a,int b);

void printer(int answer);

int main(){
	int a,b;
	scanf("%d%d",&a,&b);
    	if(a<0){
		a=a*(-1);
	}
	if(b<0){
		b=b*(-1);
	}
	printer(nod(a,b));
}

int nod(int a, int b){
	for(int i=min(a,b);i>0;i--){
		if((a%i==0)&&(b%i==0)){
			return i;
		}
	}
}

int max(int a,int b){
	if (a<b)
		return b;
	return a;
}

int min(int a,int b){
	if(a>b)
		return b;
	return a;
}

void printer(int answer){
	printf("%d\n",answer);
}


