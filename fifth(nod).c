#include <stdio.h>

int nod(int a, int b);

int max(int a,int b);

int min(int a,int b);

void printer(int answer);

int main(){
	int a,b;
	scanf("%d%d",&a,&b);
    int a1=a*a;
    a1 = a1/a;
    int b1 = b*b;
    b1=b1/b;
	printer(nod(a1,b1));
}

int nod(int a, int b){
	for(int i=min(a,b);i>0;i--){
		if((a%i==0)&&(b%i==0)){
			return i;
		}
	}
}

int max(int a,int b){
	if (a<b)
		return b;
	return a;
}

int min(int a,int b){
	if(a>b)
		return b;
	return a;
}

void printer(int answer){
	printf("%d\n",answer);
}


