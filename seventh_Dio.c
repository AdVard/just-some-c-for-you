#include <stdio.h>
#include <limits.h>


int nod(int a, int b);

int max(int a,int b);

int min(int a,int b);


void cin(int*a,int*b,int*c){
	printf("a*x + b*y = c\n");
	scanf("%d %d %d",a,b,c);
	if(*c%nod(*a,*b)!=0){
		printf("\nNet reshenia\n");
		cin(a,b,c);
	}
}

void cout(int x, int y){
	printf("%d %d\n",x, y);
}

int process(int a,int b,int c){
	for (int x = 1; a*x<INT_MAX;x++){
		if(((c-(a*x))%b)==0){
			cout(x,((c-(a*x))/b));
			return 1;
		}
	}
	return -1;
}

int main(){
	int a,b,c;
	a=0;b=0;c=0;
	cin(&a,&b,&c);
	process(a,b,c);	
}

int nod(int a, int b){
	for(int i=min(a,b);i>0;i--){
		if((a%i==0)&&(b%i==0)){
			return i;
		}
	}
}

int max(int a,int b){
	if (a<b)
		return b;
	return a;
}

int min(int a,int b){
	if(a>b)
		return b;
	return a;
}
