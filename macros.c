#include <stdio.h>
#include <stdlib.h>
#define SORT(type) void sort_##type(type arr[],int size){\
		qsort(arr,size,sizeof(type),cmp_##type);}
#define CMP(type)int cmp_##type(const void *a,const void *b){\
	return (*(type *)a-*(type *)b);}
CMP(int);
CMP(char);
CMP(double);
SORT(int);
SORT(char);
SORT(double);

int main(){
	char str[4]="BDEA";
	sort_char(str,4);
	printf("%s\n",str);
	return 0;
}
